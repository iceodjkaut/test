-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 12, 2016 at 02:56 AM
-- Server version: 10.0.27-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tom`
--

-- --------------------------------------------------------

--
-- Table structure for table `TABLE 1`
--

CREATE TABLE `TABLE 1` (
  `id` varchar(2) DEFAULT NULL,
  `aiv` varchar(25) DEFAULT NULL,
  `weight` varchar(6) DEFAULT NULL,
  `lab_germination` varchar(15) DEFAULT NULL,
  `seedling_emergence` varchar(18) DEFAULT NULL,
  `moisture_content` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TABLE 1`
--

INSERT INTO `TABLE 1` (`id`, `aiv`, `weight`, `lab_germination`, `seedling_emergence`, `moisture_content`) VALUES
('1', 'Spiderplant', '40.6', '30', '90', '11.2'),
('2', 'Nightshades', '15.6', '50', '85', '11'),
('3', 'Jute Mallow', '22.1', '75', '80', '10.2'),
('4', 'Slenderleaf(bitter taste)', '20.8', '100', '95', '9.5'),
('5', 'Slenderleaf(mild taste)', '22.3', '100', '99', '9.5'),
('6', 'Vegetables amaranths', '34', '85', '86', '9.6'),
('7', 'Vegetables cowpeas', '30.7', '100', '90', '10.9'),
('8', 'African kale', '19.7', '90', '100', '11.4');

-- --------------------------------------------------------

--
-- Table structure for table `TABLE 2`
--

CREATE TABLE `TABLE 2` (
  `AIV` varchar(18) DEFAULT NULL,
  `kisumu` varchar(6) DEFAULT NULL,
  `nairobi` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TABLE 2`
--

INSERT INTO `TABLE 2` (`AIV`, `kisumu`, `nairobi`) VALUES
('Vegetable Amaranth', '57.1', '79.7'),
('Spiderplant', '80.5', '62.5'),
('Pumpkin leaves', '32.5', '71.9'),
('Nightshade', '64.9', '89.1'),
('cowpeas leaves', '93.5', '62.5'),
('Jute Mallow', '62.3', '26.6'),
('African Kale', '27.3', '34.4'),
('Cassava', '2.6', 'null'),
('Slenderleaf', '76.6', '9.4'),
('Sweet potatoes', '3.9', 'null'),
('Stinging nettle', 'null', '18.8');

-- --------------------------------------------------------

--
-- Table structure for table `TABLE 3`
--

CREATE TABLE `TABLE 3` (
  `id` varchar(2) DEFAULT NULL,
  `AIV` varchar(20) DEFAULT NULL,
  `YIELD` varchar(5) DEFAULT NULL,
  `1000_SEED_WEIGHT` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TABLE 3`
--

INSERT INTO `TABLE 3` (`id`, `AIV`, `YIELD`, `1000_SEED_WEIGHT`) VALUES
('1', 'Spiderplant', '1100', '1.4'),
('2', 'African Nightshade', '1124', '1.2'),
('3', 'Jute Mallow', '1036', '1.9'),
('4', 'Slenderleaf (bitter)', '1272', '5.8'),
('5', 'Slenderleaf (mild)', '1172', '5.6'),
('6', 'Vegetable Amaranths', '1320', '1.1'),
('7', 'Cowpea leaves', '1120', '100');

-- --------------------------------------------------------

--
-- Table structure for table `TABLE 4`
--

CREATE TABLE `TABLE 4` (
  `id` varchar(2) DEFAULT NULL,
  `aiv` varchar(26) DEFAULT NULL,
  `raw` varchar(3) DEFAULT NULL,
  `boiled_with_iye` varchar(15) DEFAULT NULL,
  `boiled_no_Iye` varchar(13) DEFAULT NULL,
  `fried_with_Iye` varchar(14) DEFAULT NULL,
  `fried_no_Iye` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TABLE 4`
--

INSERT INTO `TABLE 4` (`id`, `aiv`, `raw`, `boiled_with_iye`, `boiled_no_Iye`, `fried_with_Iye`, `fried_no_Iye`) VALUES
('1', 'Nightshade', '17.', '12.4', '11.5', '281.2', '401.6'),
('2', 'Cowpeas', '24.', '20.6', '15.3', '16.7', '1208'),
('3', 'Slenderleaf', '14.', '10.7', '6.4', '110', '5.8'),
('4', 'Amaranth', '19.', '95.2', '12.4', '108', '5.3'),
('5', 'Nightshade and Amaranth', '20.', '11.5', '11.5', '9.1', '42.3'),
('6', 'Nightshade and Slenderleaf', '13.', '12.2', '13.9', '5.1', '8.2'),
('7', 'Nightshade and Cowpea', '23.', '69.2', '200.4', '381.4', '6.6'),
('8', 'Amaranth and Slenderleaf', '12.', '23.6', '557.5', '859.2', '859.2'),
('9', 'Amaranth and Cowpea', '20.', '69.2', '16.6', '7.2', '303'),
('10', 'Slenderleaf and Cowpea', '14.', '26.9', '9.1', '10.1', '8.1');

-- --------------------------------------------------------

--
-- Table structure for table `TABLE 5`
--

CREATE TABLE `TABLE 5` (
  `id` varchar(2) DEFAULT NULL,
  `canopy` varchar(16) DEFAULT NULL,
  `ipm` varchar(4) DEFAULT NULL,
  `no_rust` varchar(7) DEFAULT NULL,
  `no_thrips` varchar(9) DEFAULT NULL,
  `all_control` varchar(11) DEFAULT NULL,
  `no_control` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TABLE 5`
--

INSERT INTO `TABLE 5` (`id`, `canopy`, `ipm`, `no_rust`, `no_thrips`, `all_control`, `no_control`) VALUES
('1', 'Pure Stand', '10.7', '9.18', '11.53', '7.79', '5.45'),
('2', 'Alternate rows', '10.7', '8.16', '7.99', '10.06', '8.23'),
('3', 'Alternate strips', '10.5', '5.68', '10.6', '8.84', '9.1');

-- --------------------------------------------------------

--
-- Table structure for table `TABLE 6`
--

CREATE TABLE `TABLE 6` (
  `id` varchar(2) DEFAULT NULL,
  `aiv` varchar(18) DEFAULT NULL,
  `protein` varchar(7) DEFAULT NULL,
  `calcium` varchar(4) DEFAULT NULL,
  `iron` varchar(13) DEFAULT NULL,
  `beta_carotene` varchar(9) DEFAULT NULL,
  `vitamic_c` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TABLE 6`
--

INSERT INTO `TABLE 6` (`id`, `aiv`, `protein`, `calcium`, `iron`, `beta_carotene`, `vitamic_c`) VALUES
('1', 'African nightshade', '4.7', '442', '26.4', '8.8', '131'),
('2', 'Vegetable Amaranth', '4.6', '480', '19', '6.8', '117'),
('3', 'Cowpea', '4.8', '152', '39', '5.7', '87'),
('4', 'Spiderplant', '5.1', '262', '19', '8.7', '144'),
('5', 'Jute Mallow', '4.5', '360', '7.7', '6.4', '187'),
('6', 'Pumpkin leaves', '2.7', '477', '50', '3.6', '80'),
('7', 'slenderleaf', '9.47', '18.4', '13.9', '8.6', '20'),
('8', 'Vine Spinach', '1.8', '109', '12', '8', '102'),
('9', 'Common cabbage', '1.4', '44', '0.8', '1.2', '33');

-- --------------------------------------------------------

--
-- Table structure for table `TABLE 7`
--

CREATE TABLE `TABLE 7` (
  `id` varchar(2) DEFAULT NULL,
  `Amarantha variety` varchar(24) DEFAULT NULL,
  `moisture` varchar(8) DEFAULT NULL,
  `calcium` varchar(7) DEFAULT NULL,
  `iron` varchar(4) DEFAULT NULL,
  `vitamin_c` varchar(9) DEFAULT NULL,
  `beta_carotene` varchar(10) DEFAULT NULL,
  `flavanoids` varchar(26) DEFAULT NULL,
  `anti_oxidant_activity_IC50` varchar(26) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TABLE 7`
--

INSERT INTO `TABLE 7` (`id`, `Amarantha variety`, `moisture`, `calcium`, `iron`, `vitamin_c`, `beta_carotene`, `flavanoids`, `anti_oxidant_activity_IC50`) VALUES
('1', 'Abuku amaranth 1', '87.7', '281.1', '22.8', '117.1', '6.7', '4284', '1.2'),
('2', 'Abuku amaranth 2', '87.4', '288.3', '32.1', '74.8', '6.7', '3368.1', '1.5'),
('3', 'Abuku amaranth 3', '87.4', '399.7', '26.6', '68.6', '5.4', '3629.9', '1'),
('4', 'Abuku amaranth 4', '87.6', '270.9', '23.9', '76.6', '5.4', '3026.5', '1.3'),
('5', 'Abuku amaranth 5', '86.7', '225.2', '35.4', '85.1', '6.7', '3170.1', '1.2'),
('6', 'Abuku amaranth 6', '87.6', '315.6', '30.9', '82.1', '6.8', '3002.9', '1.3'),
('7', 'Abuku amaranth 7', '87.5', '353', '33.7', '66.4', '6.2', '2819.9', '1.2'),
('8', 'Abuku amaranth 8', '87.4', '314.1', '31.6', '88.8', '6.5', '3201', '1.4'),
('9', 'Standard variety 1', '87.6', '289.9', '25.7', '85.5', '5.6', '3048.9', '1.1'),
('10', 'Standard variety 2', '87.6', '312.8', '26.5', '84.9', '5.3', '2994.8', '1'),
('11', 'Accession 1', '86.1', '10.2', '9.9', '19', '8.59', 'null', 'null'),
('12', 'Accession 2', '85.3', '12.5', '10.4', '12.4', '8.56', 'null', 'null'),
('13', 'Accession 8', '89.4', '2.6', '13.9', '18.5', '8.55', 'null', 'null'),
('14', 'Accession 11', '88.1', '2.5', '12.4', '10.6', '8.67', 'null', 'null'),
('15', 'Accession 14', '84.6', '17.7', '10.7', '11.5', '7.32', 'null', 'null'),
('16', 'Accession 18', '90.2', '13.6', '8.96', '11.5', '7.35', 'null', 'null'),
('17', 'Abuku Slenderleaf 2 (19)', '87.9', '2.1', '12.8', '12.2', '7.56', 'null', 'null'),
('18', 'Accession 20', '88.2', '2.7', '10.7', '11.6', '7.73', 'null', 'null'),
('19', 'Accession 25', '82.8', '13.3', '8.1', '11.2', '8.15', 'null', 'null'),
('20', 'Abuku Slenderleaf 1 (33)', '82.7', '18.4', '10.2', '14.2', '7.98', 'null', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `TABLE 9`
--

CREATE TABLE `TABLE 9` (
  `COL 1` varchar(2) DEFAULT NULL,
  `COL 2` varchar(19) DEFAULT NULL,
  `COL 3` varchar(8) DEFAULT NULL,
  `COL 4` varchar(9) DEFAULT NULL,
  `COL 5` varchar(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `TABLE 9`
--

INSERT INTO `TABLE 9` (`COL 1`, `COL 2`, `COL 3`, `COL 4`, `COL 5`) VALUES
('id', 'aiv', 'Kakamega', 'chavakali', 'kiboswa'),
('1', 'Cowpeas', '30', '18', '31'),
('2', 'Leaf Amaranths', '21', '18', '15'),
('3', 'African Nightshades', '12', '14', '15'),
('4', 'Jute Mallow', '11', '18', '15'),
('5', 'Spiderplant', '7', '18', '16'),
('6', 'Slenderleaf', '7', '14', '8'),
('7', 'African kale', '7', 'null', 'null'),
('8', 'Pumpkin leaves', '5', 'null', 'null');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
